/*
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these * multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */



console.log("seq:" + seqSum(1000));
console.log("rec:" + recSum(1, 0));
console.log("sum:" + (sumOfMultiples(3) + sumOfMultiples(5) - sumOfMultiples(15)));
console.log("req:" + rec_sumOfMultiples(3, 1, 0) + rec_sumOfMultiples(5, 1, 0) + rec_sumOfMultiples(15, 1, 0));

function seqSum(max) {
	var sum = 0;

	for (var i = 1; i < max; i++) {
		if (i % 3 === 0 || i % 5 === 0) {
	 		sum += i;
	 	}
	}

	return sum;	
}

function recSum(nextInput, sum) {
	if (nextInput >= 1000) {
		return sum;
	}

	if (nextInput % 3 === 0 || nextInput % 5 === 0) {
		sum += nextInput;
	}
		
	nextInput++;

	return recSum(nextInput, sum);
}

// Okay that's fine so far, but I can mimize the time complexity, can we reduce it to a minimum.
// add up all 3's multiples plus all 5's multiples and minus 3 * 5 multiples

function sumOfMultiples(multiple) {
	var i = 1;
	var sum = 0;

	while (multiple * i < 1000) {
		sum += multiple * i;
		i++;
	}

	return sum;
}

// Recursive, 
// 1. think about the base conditions that terminate the recursion
// 2. the content of the function or functionality
// 3. call the function itself with different value that can push the function forwarad to the base conditions

function rec_sumOfMultiples(multiple, i, sum) {
	if (multiple * i >= 1000) {
		return sum;
	}

	sum += multiple * i;
	i++;

	return rec_sumOfMultiples(multiple, i, sum);
}